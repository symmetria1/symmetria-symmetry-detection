#  Symmetry detection on Symmetria
This repository contains the implementation, configuration files and pre-trained checkpoints for the project described in the paper: Symmetria: A Synthetic Dataset for Learning in Point
Clouds

## Setup
Clone the repository and install the required dependencies:

```
git clone https://github.com/QwagPerson/symmetria-symmetry-detection
cd symmetria-symmetry-detection
pip install -r requirements.txt
```

## Usage
This project uses PyTorch Lightning to streamline the training and testing processes. 
To train or test the model, modify the configuration file as needed and execute the main.py script using the PyTorch Lightning API.

### Training
```
python main.py fit --config <PATH/TO/CONFIG>
```

### Evaluation
```
python main.py test --config <PATH/TO/CONFIG> --ckpt_path <PATH/TO/CKPT>
```

## License
This project is licensed under the MIT License. See the LICENSE file for details.
Citation
